<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <?php wp_head(); ?>
  <meta charset="<?php echo get_bloginfo('charset') ?>" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title><?php echo bloginfo('name') ?></title>
  <link rel="shortcut icon" href="<?php echo get_template_directory_uri() ?>/assets/vector/favicon.svg" type="image/x-icon" />
  <link rel="stylesheet" media="all" href="<?php echo get_template_directory_uri() ?>/assets/css/libs/swiper.css">
  <link rel="stylesheet" media="all" href="<?php echo get_template_directory_uri() ?>/assets/css/libs/bootstrap-grid.css">
  <link rel="stylesheet" media="all" href="<?php echo get_template_directory_uri() ?>/assets/css/home.css">
</head>

<body <?php body_class(); ?>>
  