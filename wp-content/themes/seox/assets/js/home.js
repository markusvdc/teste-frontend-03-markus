// ICONS
const icon_play = '<svg viewBox="0 0 60 60" xml:space="preserve"><path d="M30,0C13.4,0,0,13.4,0,30c0,16.6,13.4,30,30,30c16.6,0,30-13.4,30-30C60,13.4,46.6,0,30,0z M39.8,30.3c0,0.1-0.1,0.1-0.2,0.2L25,41.1c-0.1,0.1-0.2,0.1-0.3,0.1c-0.1,0-0.2,0-0.3-0.1s-0.2-0.1-0.2-0.2c-0.1-0.1-0.1-0.2-0.1-0.3V19.4c0-0.1,0-0.2,0.1-0.3c0.1-0.1,0.1-0.2,0.2-0.2c0.1,0,0.2-0.1,0.3-0.1c0.1,0,0.2,0,0.3,0.1l14.6,10.6c0.1,0,0.1,0.1,0.2,0.2c0,0.1,0.1,0.2,0.1,0.2S39.9,30.2,39.8,30.3z"/></svg>'

const icon_comment = '<svg viewBox="0 0 18 15" xml:space="preserve"><path d="M16.7,12.1c0.8-0.9,1.3-2,1.3-3.2c0-2.8-2.5-5-5.6-5.2c0.1,0.4,0.2,0.9,0.2,1.4l0,0c2.3,0.2,4.1,1.8,4.1,3.7c0,1-0.5,1.8-0.9,2.3l-0.7,0.7l0.4,0.9c0.1,0.2,0.2,0.4,0.3,0.6c-0.5-0.1-1-0.4-1.5-0.7l-0.5-0.3l-0.5,0.1c-0.4,0.1-0.8,0.2-1.2,0.2c-1.6,0-3.1-0.7-3.9-1.8c-0.4,0.2-0.9,0.3-1.4,0.3c1,1.7,2.9,2.9,5.3,2.9c0.5,0,1-0.1,1.5-0.2c0.9,0.6,2.1,1.1,3.6,1.1c0.3,0,0.5-0.2,0.6-0.4c0.1-0.3,0.1-0.6-0.1-0.8C17.8,13.8,17.2,13,16.7,12.1z"/><path d="M11.7,5.2C11.7,2.3,9.1,0,5.8,0C2.6,0,0,2.3,0,5.2c0,1.2,0.5,2.3,1.3,3.2C0.8,9.3,0.2,10,0.2,10c-0.2,0.2-0.2,0.5-0.1,0.8c0.1,0.3,0.4,0.4,0.6,0.4c1.5,0,2.8-0.6,3.6-1.1c0.5,0.1,1,0.2,1.5,0.2C9.1,10.3,11.7,8,11.7,5.2z M5.8,8.9c-0.4,0-0.8,0-1.2-0.2L4.1,8.6L3.6,8.9c-0.5,0.3-1,0.6-1.5,0.7C2.3,9.4,2.4,9.2,2.5,9l0.4-0.9L2.3,7.4C1.8,6.9,1.3,6.2,1.3,5.2c0-2.1,2-3.8,4.5-3.8c2.5,0,4.5,1.7,4.5,3.8S8.3,8.9,5.8,8.9z"/></svg>'

// FEED REQUEST
const swiper_wraper = document.getElementById('swiper_wraper')

fetch('https://teste-frontend.seox.com.br/wp-json/wp/v2/posts?_embed')
  .then(response => response.json())
  .then(response => {
    // CREATE ITEMS
    response.forEach((item, index) => {

      // INFORMATION
      const feed_image = item['_embedded']['wp:featuredmedia'][0]['media_details']['sizes']['full']['source_url']
      const feed_hat = item['acf']['hat']
      const feed_title = item['title']['rendered']

      swiper_wraper.innerHTML += `
      <div class="swiper-slide feed__slide">
      <div class="feed__info1">
        <img class="feed__image" src="${feed_image}" alt="">
        <button class="feed__play">
          ${icon_play}
        </button>
        <div class="feed__comment">
          ${icon_comment}29
        </div>
      </div>
        <div class="feed__info2">
          <div class="feed__hat">${feed_hat}</div>
          <h2 class="feed__title">${feed_title}</h2>
        </div>
      </div>
      `
    })
  })
  .then(() => {
    // FEED SLIDER
    const feed_slider = new Swiper('.feed__swiper', {
      slidesPerView: 'auto',
      spaceBetween: 19,
      speed: 600,
      navigation: {
        nextEl: '.videos__arrow--next',
        prevEl: '.videos__arrow--prev',
      },
      breakpoints: {
        1200: {
          direction: 'vertical',
        }
      }
    })
  })
  .catch(err => {
    swiper_wraper.innerHTML += 'Mensagem de erro'
  })