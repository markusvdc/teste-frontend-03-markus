<?php get_header(); ?>

<!-- VIDEOS -->
<section class="videos">
  <div class="videos__container container">
    <div class="videos__header">
      <h1 class="videos__title">Vídeos</h1>
      <div class="videos__buttons">
        <a href="#" class="videos__more">
          Veja mais <?php echo file_get_contents(get_template_directory_uri() . '/assets/vector/arrow.svg') ?>
        </a>
        <button class="videos__arrow videos__arrow--prev"><?php echo file_get_contents(get_template_directory_uri() . '/assets/vector/arrow.svg') ?></button>
        <button class="videos__arrow videos__arrow--next"><?php echo file_get_contents(get_template_directory_uri() . '/assets/vector/arrow.svg') ?></button>
      </div>
    </div>
    <div class="videos__content">
      <div class="row">
        <!-- LAST -->
        <div class="col-xl">
          <article class="last">
            <img class="last__image" src="<?php echo get_template_directory_uri() . '/assets/img/last.webp' ?>" width="1029" height="642" alt="">
            <div class="last__info">
              <button class="last__play"><?php echo file_get_contents(get_template_directory_uri() . '/assets/vector/play.svg') ?></button>
              <div class="last__hat">Lorem ipsum</div>
              <h2 class="last__title">Lorem ipsum dolor sit amet consectetur adipiscing elit ut nunc nulla accumsan id</h2>
            </div>
          </article>
        </div>

        <!-- FEED -->
        <div class="col-xl-3">
          <div class="feed">
            <div class="swiper feed__swiper">
              <div class="swiper-wrapper" id="swiper_wraper">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>