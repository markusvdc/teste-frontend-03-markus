<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do banco de dados
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do banco de dados - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'seox' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '/|^@xa>4%KCM0MeJ]xU3m@1UHNX~uJNaS@Wd&FT{gruGA~Ge6-?Z@$o$RL0s0kDP' );
define( 'SECURE_AUTH_KEY',  '5WnXvM40YY-Zteq-0+>R1S<{*)y9,ci5Ep(lGlOFd#1-nO01f#qj/HdR7fU2|ihl' );
define( 'LOGGED_IN_KEY',    ';TdmCKD]C:qw0zw.DQX?h<WaFq}{zekM$f.v<_8~fF5OYRX 3h?sDn-)nyP E2q)' );
define( 'NONCE_KEY',        '< T,>K/}([e^bJ2a[/fVD2IwDu1{tk{Wq8,]9]!nV)Q-<$%>qRuTXh=m3.OSn#53' );
define( 'AUTH_SALT',        ']R)$$WsI$U*+KUCvkOhE}q1w9pP8L@cgnzH32jMgyRSCeh$&M@u^}*86|bJK*A2-' );
define( 'SECURE_AUTH_SALT', '186P:uKI%~mm~FM^]&oq#;BY/$LRFRr*[QS=*[I]3VsnQOYy9^>-N*NJ}~![(1l}' );
define( 'LOGGED_IN_SALT',   '87]&iGaSEE^wOfTVqLBUgh]a*5GswXn@08!-&Z26W/#P89/Br,)B[:(Z4{.h}YGw' );
define( 'NONCE_SALT',       'K,c{j,&4kl:uD>+6]@`=5o]76F?N&Jy/l{r{slOq5a30y$hEjP$>ftV@-~)3dcW)' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_seox_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Adicione valores personalizados entre esta linha até "Isto é tudo". */



/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';

// Disable system updates
define( 'WP_AUTO_UPDATE_CORE', false );
